Tres en Raya
============

Basado en el juego clásico del [Tres en línea][1]. 
Puede [probarlo ahora mismo][2] y leer este aburrido README más tarde.

Descripción
-----------

  * El juego debe componerse de una tabla 3x3.
  * Dos jugadores, uno representado por la marca O y el otro por X.
  * El primer jugador en alcanzar 3 marcas seguidas, ya sea horizontal, vertical o en diagonal gana.
  * Los jugadores juegan en el mismo ordenador, una vez clica uno le toca al otro, no se necesita instalación previa.

Requisitos
----------

  * Toda la lógica debe estar escrita en PHP.
  * La programación debe ser orientada a objetos.
  * Utiliza una base de datos para almacenar el estado del juego.
  * El frontend puede ser simple pero fácil para identificar la tabla 3x3.

Extras
------

  * `Symfony 4` con Twig Templates y Doctrine.
  * CSS generado con `SASS`.
  * Dos versiones del juego: `Sesión` y `Base de datos`.

Uso
---
Instalar (si no se tiene ya) la aplicación [Symfony CLI][3], lanzar el servidor desde la consola y acceder a la aplicación desde el navegador <http://localhost:8000>:

```bash
$ cd 3en-/
$ symfony serve:start
```  

Cerrar presionando `CTRL+C`.
Para la versión `Sesión` no se precisa la creación de la base de datos.

  
Notas adicionales
-----------------

  * Desarrollado con PHP 7.3.4. No se ha probado en versiones inferiores.

Mejoras
-------

  * Añadir un timestamp sobre las partidas de la base de datos para eliminar las antiguas con un cronjob.
  * Registrar cuantas partidas han ganado y empatado los jugadores.
  

[1]: https://es.wikipedia.org/wiki/Tres_en_l%C3%ADnea
[2]: http://3-en-raya.alia2.es/
[3]: https://symfony.com/download
