<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartidaRepository")
 */
class Partida
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $estado = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEstado(): ?array
    {
        return $this->estado;
    }

    public function setEstado(array $estado): self
    {
        $this->estado = $estado;

        return $this;
    }
}
