<?php
namespace App\Utils;

/**
 * 24/04/2019
 * Juego del Tres en raya.
 *
 * @author Agustín R.R. <agustin.rr.job@gmail.es>
 * Copyright (c) 2019
 */
class Juego
{
  /* ========================================================================
   * Constantes
   * ======================================================================== */


  /* ========================================================================
   * Atributos
   * ======================================================================== */

    /**
     * Array con las 9 celdas del tablero. La posición 0 es la superior
     * izquierda, la 1 la superior central,...
     *
     * Cada celda puede tener 3 valores:
     *   · 1: Ficha del jugador 1
     *   · 0: Celda libre
     *   · 2: Ficha del jugador 2
     *
     * @var array int
     */
    private $celdas;

    /**
     * Si un jugador ha ganado contiene la posición de las celdas que forman
     * las 3 en raya. Hasta entonces contiene un array vacío
     *
     * @var array int
     */
    private $celdasEnRaya;

    /**
     * ¿A qué jugador le toca poner ficha?
     * Valores válidos: 1 y 2. Por defecto comienza el 1.
     *
     * @var int
     */
    private $jugadorActivo;



  /* ========================================================================
   * Constructor
   * ======================================================================== */

  function __construct() {
      $this->ini();
  }



  /* ========================================================================
   * Métodos
   * ======================================================================== */

  private function ini()
  {
      $this->celdas= [
        0, 0, 0,
        0, 0, 0,
        0, 0, 0
      ];

      $this->celdasEnRaya= [];
      $this->jugadorActivo= 1;
  }


  public function reinicia()
  {
      $this->ini();
  }


  public function getCeldas(): array
  {
      return $this->celdas;
  }


  public function getCeldasEnRaya(): array
  {
      return $this->celdasEnRaya;
  }


  public function getJugadorActivo(): int
  {
      return $this->jugadorActivo;
  }

  /**
   * @return int|null
   */
  public function getJugadorGanador(): ?int
  {
      return (count($this->celdasEnRaya) > 0) ? $this->celdasEnRaya[0] : null;
  }


  /**
   * @param int $idCelda
   */
  public function isCeldaLibre(int $idCelda): bool
  {
      return key_exists($idCelda, $this->celdas) &&
             ($this->celdas[$idCelda] === 0);
  }


  public function isCeldaEnRaya(int $idCelda): bool
  {
      return in_array($idCelda, $this->celdasEnRaya);
  }


  /**
   * ¿Las 3 celdas tienen fichas del mismo jugador?
   */
  private function isTresEnRaya(
      int $idCelda1,
      int $idCelda2,
      int $idCelda3): bool
  {
      return ($this->celdas[$idCelda1] !== 0) &&
             ($this->celdas[$idCelda1] === $this->celdas[$idCelda2]) &&
             ($this->celdas[$idCelda2] === $this->celdas[$idCelda3]);
  }


  /**
   * Comprueba si hay celdas en raya.
   */
  private function actualizaCeldasEnRaya()
  {
      // Comprueba las líneas horizontales
      if ($this->isTresEnRaya(0, 1, 2)) {
          $this->celdasEnRaya= [0, 1, 2];
      }

      else if ($this->isTresEnRaya(3, 4, 5)) {
          $this->celdasEnRaya= [3, 4, 5];
      }

      else if ($this->isTresEnRaya(6, 7, 8)) {
          $this->celdasEnRaya= [6, 7, 8];
      }

      // Comprueba las líneas verticales
      else if ($this->isTresEnRaya(0, 3, 6)) {
          $this->celdasEnRaya= [0, 3, 6];
      }

      else if ($this->isTresEnRaya(1, 4, 7)) {
          $this->celdasEnRaya= [1, 4, 7];
      }

      else if ($this->isTresEnRaya(2, 5, 8)) {
          $this->celdasEnRaya= [2, 5, 8];
      }

      // Comprueba las diagonales
      else if ($this->isTresEnRaya(0, 4, 8)) {
          $this->celdasEnRaya= [0, 4, 8];
      }

      else if ($this->isTresEnRaya(2, 4, 6)) {
          $this->celdasEnRaya= [2, 4, 6];
      }
  }


  public function getNumCeldasLibres(): int
  {
      $cnt= 0;
      foreach ($this->celdas as $valor) {
          if ($valor === 0) {
              ++ $cnt;
          }
      }

      return $cnt;
  }


  public function isInicio(): bool
  {
    $numLibres= $this->getNumCeldasLibres();
    return ($numLibres === 9);
  }


  public function isJuegoTerminado(): bool
  {
    // ¿Hay celdas en raya?
    if ( ! empty($this->celdasEnRaya)) {
        return true;
    }

    // ¿No quedan celdas libres?
    $numLibres= $this->getNumCeldasLibres();
    return ($numLibres === 0);
  }


  /**
   * Si hay un ganador será el jugador activo, sino lo averigua por el nº de
   * celdas libres.
   */
  private function actualizaJugadorActivo()
  {
      // Si ya hay un ganador será el jugador activo
      if ( ! empty($this->celdasEnRaya))
      {
          $this->jugadorActivo= $this->celdas[ $this->celdasEnRaya[0] ];
      }
      else
      {
          $numLibres= $this->getNumCeldasLibres();
          $this->jugadorActivo= ($numLibres % 2 === 0) ? 2 : 1;
      }
  }


  /**
   * La disposición de fichas en el tablero ha cambiado. Actualiza las celdas
   * en raya y el jugador activo.
   */
  private function actualizaEstado()
  {
      $this->actualizaCeldasEnRaya();
      $this->actualizaJugadorActivo();
  }


  /**
   * El jugador activo quiere poner una ficha en $idCelda.
   *
   * @param int $idCelda
   * @return bool ¿Ha cambiado el tablero?
   */
  public function pulsa(int $idCelda): bool
  {
      if ( ! $this->isCeldaLibre($idCelda)) {
          return false;
      }

      // Pone la ficha
      $this->celdas[$idCelda]= $this->jugadorActivo;

      // Actualiza el estado actual del juego
      $this->actualizaEstado();

      return true;
  }


  /**
   * Actualiza el juego con un tablero concreto.
   *
   * @param array $celdas
   */
  public function actualiza(array $celdas)
  {
      $this->celdas= $celdas;

      // Actualiza el estado actual del juego
      $this->actualizaEstado();
  }


  /* ========================================================================
   * Métodos estáticos
   * ======================================================================== */

}
?>