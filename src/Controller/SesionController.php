<?php
namespace App\Controller;

use App\Utils\Juego;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * 24/04/2019
 * Controlador principal del Tres en raya.
 * Trabaja sólo con la sesión.
 *
 * @author Agustín R.R. <agustin.rr.job@gmail.es>
 * Copyright (c) 2019
 */
class SesionController extends AbstractController
{

    /**
     * @Route("/sesion/", name="sesion-inicio", methods={"GET"})
     */
    public function principal(SessionInterface $session)
    {
        if ($session->has('juego')) {
            $juego= $session->get('juego');
        }
        else {
            $juego= new Juego();
            $session->set('juego', $juego);
        }

        return $this->render('juego/principal.html.twig', [
            'contexto' => 'sesion',
            'juego' => $juego
        ]);
    }


    /**
     * @Route("/sesion/reinicia-partida", name="sesion-reinicia", methods={"GET"})
     */
    public function reiniciaPartida(SessionInterface $session)
    {
        if ($session->has('juego')) {
          $session->remove('juego');
        }

        // Redirige a la página principal
        return $this->redirectToRoute('sesion-inicio');
    }


    /**
     * @Route("/sesion/pulsa/{idCelda}", name="sesion-pulsa", methods={"GET"})
     */
    public function pulsa($idCelda, SessionInterface $session)
    {
        if ($session->has('juego'))
        {
          /* @var $juego Juego */
          $juego= $session->get('juego');

          $juego->pulsa($idCelda);
        }

        // Redirige a la página principal
        return $this->redirectToRoute('sesion-inicio');
    }


}
