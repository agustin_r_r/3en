<?php
namespace App\Controller;

use App\Entity\Partida;
use App\Utils\Juego;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * 24/04/2019
 * Controlador principal del Tres en raya.
 * Trabaja con la Base de datos.
 *
 * @author Agustín R.R. <agustin.rr.job@gmail.es>
 * Copyright (c) 2019
 */
class BdController extends AbstractController
{

    /**
     * Crea una partida nueva.
     *
     * @Route("/bd/", name="bd-inicio", methods={"GET"})
     */
    public function principal()
    {
        $juego= new Juego();

        $entityManager = $this->getDoctrine()->getManager();

        $partida= new Partida();
        $partida->setEstado($juego->getCeldas());

        $entityManager->persist($partida);
        $entityManager->flush();

        // Redirige a la página que muestra la partida
        return $this->redirectToRoute('bd-partida', [
          'id' => $partida->getId()
        ]);
    }


    /**
     * @Route("/bd/{id}", name="bd-partida", methods={"GET"})
     */
    public function partida(Partida $partida)
    {
        $juego= new Juego();
        $juego->actualiza($partida->getEstado());

        return $this->render('juego/principal.html.twig', [
            'contexto' => 'bd',
            'idPartida' => $partida->getId(),
            'juego' => $juego
        ]);
    }


    /**
     * @Route("/bd/reinicia-partida/{id}", name="bd-reinicia", methods={"GET"})
     */
    public function reiniciaPartida(Partida $partida)
    {
        // Aprovecha el registro de la tabla para crear una partida nueva
        $juego= new Juego();
        $partida->setEstado($juego->getCeldas());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        // Redirige a la página que muestra la partida
        return $this->redirectToRoute('bd-partida', [
          'id' => $partida->getId()
        ]);
    }


    /**
     * @Route("/bd/pulsa/{id}/{idCelda}", name="bd-pulsa", methods={"GET"})
     */
    public function pulsa(Partida $partida, $idCelda)
    {
        $juego= new Juego();
        $juego->actualiza($partida->getEstado());

        // Si cambia el tablero con la pulsación
        if ($juego->pulsa($idCelda))
        {
          $partida->setEstado($juego->getCeldas());

          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->flush();
        }

        // Redirige a la página que muestra la partida
        return $this->redirectToRoute('bd-partida', [
          'id' => $partida->getId()
        ]);
    }


}
