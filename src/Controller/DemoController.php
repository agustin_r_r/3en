<?php
namespace App\Controller;
// use Symfony\Component\HttpFoundation\Response;

use App\Utils\Juego;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * 24/04/2019
 * Controlador para mostrar posibles partidas del Tres en raya.
 *
 * @author Agustín R.R. <agustin.rr.job@gmail.es>
 * Copyright (c) 2019
 */
class DemoController extends AbstractController
{

    /**
     * Comienzo de partida.
     *
     * @Route("/demo/partida-inicio")
     */
    public function partidaInicio()
    {
        $juego= new Juego();

        return $this->render('juego/principal.html.twig', [
            'juego' => $juego
        ]);
    }


    /**
     * Le toca al jugador 1.
     *
     * @Route("/demo/partida-juega-1")
     */
    public function partidaJuega1()
    {
        $juego= new Juego();

        $celdas= [
          1, 1, 2,
          0, 2, 0,
          0, 0, 0
        ];
        $juego->actualiza($celdas);

        return $this->render('juego/principal.html.twig', [
            'juego' => $juego
        ]);
    }


    /**
     * Le toca al jugador 2.
     *
     * @Route("/demo/partida-juega-2")
     */
    public function partidaJuega2()
    {
        $juego= new Juego();

        $celdas= [
          1, 1, 2,
          0, 2, 0,
          1, 0, 0
        ];
        $juego->actualiza($celdas);

        return $this->render('juego/principal.html.twig', [
            'juego' => $juego
        ]);
    }


    /**
     * @Route("/demo/partida-empate")
     */
    public function partidaEmpate()
    {
        $juego= new Juego();

        $celdas= [
          1, 1, 2,
          2, 2, 1,
          1, 1, 2
        ];
        $juego->actualiza($celdas);

        return $this->render('juego/principal.html.twig', [
            'juego' => $juego
        ]);
    }


    /**
     * Gana el jugador 1.
     *
     * @Route("/demo/partida-gana-1")
     */
    public function partidaGana1()
    {
        $juego= new Juego();

        $celdas= [
          1, 0, 2,
          2, 1, 0,
          0, 0, 1
        ];
        $juego->actualiza($celdas);

        return $this->render('juego/principal.html.twig', [
            'juego' => $juego
        ]);
    }

}
